""" Read data from csv file input_file.
Assign a unique index to each team and then 
add two columns Hindex and Aindex to contain
the indices for the home and away team.
"""

import pandas as pd
import numpy as np

input_file = 'football_dataset.csv'
input_path = '../data/' + input_file


def homeclubindex(x):
    return clubs.index(x.Hclub)


def awayclubindex(x):
    return clubs.index(x.Aclub)


df = pd.read_csv(input_path, index_col=0)
clubs = list(np.union1d( df['Aclub'].unique(), df['Hclub'].unique()))
df['Hindex'] = df.apply(homeclubindex, axis=1)
df['Aindex'] = df.apply(awayclubindex, axis=1)
