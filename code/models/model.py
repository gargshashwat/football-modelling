from abc import ABCMeta, abstractmethod


class Model(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def train_model(self, df):
        """ train the model and set the parameters of the model """
        print("not implemented trainModel")

    def probabilities(self, row):
        """ return probabilities for [p_H, p_D, p_A, p_U, p_O] """
        print("not implemented probabilities")
