""" Poissons Model """

from models.model import Model
import numpy as np
from scipy.optimize import minimize
from scipy.stats import poisson
import warnings


class Poissons(Model):
    def __init__(self, independent=True):
        warnings.filterwarnings('error')
        self.independent = independent
        self.name = "Independent Poissons"
        if self.independent == False:
            self.name = "Bivariate Poissons"
        
    def frank_copula(self, u, v, kappa):
        try:
            a = (np.exp(-kappa*u)-1)*(np.exp(-kappa*v)-1) / (np.exp(-kappa)-1)

            return (-1/kappa) * np.log(1+a)
        except Warning:
            return u*v

    def likelihood(self, l_x, x, l_y, y, kappa):
        a = poisson.cdf(x, l_x)
        b = poisson.cdf(y, l_y)
        c = poisson.cdf(x-1, l_x)
        d = poisson.cdf(y-1, l_y)

        ret = (self.frank_copula(a, b, kappa) - self.frank_copula(c, b, kappa) 
        - self.frank_copula(a, d, kappa) + self.frank_copula(c, d, kappa))

        return ret
   
    def nll(self, alpha, beta, gamma, kappa, I, J, X, Y):
        lambda_X = np.exp(alpha[I] + beta[J] + gamma)
        lambda_Y = np.exp(alpha[J] + beta[I])

        if self.independent:
            return -np.sum(poisson.logpmf(X, lambda_X) + poisson.logpmf(Y, lambda_Y))
        
        ret = self.likelihood(lambda_X, X, lambda_Y, Y, kappa)

        if 0 in ret:
            index = np.where(ret==0)
            ind = index[0][0]
            print(kappa)
            print(lambda_X[ind])
            print(lambda_Y[ind])

        return -np.sum(np.log(ret))

    def model(self, params):
        gamma = params[0]
        alpha = params[1: len(self.clubs)+1]
        beta = params[len(self.clubs)+1: -1]
        kappa = params[len(self.clubs)]
        
        return self.nll(alpha, beta, gamma, kappa, 
                        self.train['Hindex'], self.train['Aindex'], 
                        self.train['FTHG'], self.train['FTAG'])
    
    def train_model(self, df, clubs):
        self.clubs = clubs
        self.train = df

        params = list(np.zeros(1+2*len(self.clubs)+1))   # params = gamma, alpha, beta, kappa
        
        cons = []
        #cons.append({'type': 'eq', 'fun': lambda x:  np.sum(x[1:1+len(self.clubs)])})  # to ensure a unique solution
        if self.independent:
            cons.append({'type': 'eq', 'fun': lambda x:  x[len(self.clubs)]}) # kappa=0 if independent
        else:
            upper_bound = 30
            cons.append({'type': 'ineq', 'fun': lambda x:  upper_bound - x[len(self.clubs)] }) # putting an upper bound on kappa to avoid numerical issues
            
        def poi_lambda(x):
            gamma = x[0]
            alpha = x[1: 1+len(self.clubs)]
            beta = x[1+len(self.clubs): -1]

            I = self.train['Hindex']
            J = self.train['Aindex']

            loglambda_X = alpha[I] + beta[J] + gamma
            loglambda_Y = alpha[J] + beta[I]

            return np.concatenate((loglambda_X, loglambda_Y)) 

        upper_bound = np.log(4)
        lower_bound = np.log(0.01)
        cons.append({'type': 'ineq', 'fun': lambda x:  upper_bound-poi_lambda(x) }) # to bound each parameter from above
        cons.append({'type': 'ineq', 'fun': lambda x:  poi_lambda(x)-lower_bound }) # to bound each parameter from below
        
        res_model = minimize(self.model, params, constraints=cons)
                
        self.gamma = res_model.x[0]
        self.alpha = res_model.x[1: 1+len(self.clubs)]
        self.beta = res_model.x[1+len(self.clubs): -1]
        self.kappa = res_model.x[len(self.clubs)]

    def probabilities(self, x):
        i = x['Hindex']  
        j = x['Aindex']
        
        bound = 9
        distribution = np.zeros(2*bound+1)
        under_prob = 0
        over_prob = 0
        
        lambda_x = np.exp(self.alpha[i] + self.beta[j] + self.gamma)
        lambda_y = np.exp(self.alpha[j] + self.beta[i])
        
        for hg in range(0, bound+1):
            for ag in range(0, bound+1):
                prob = self.likelihood(lambda_x, hg, lambda_y, ag, self.kappa)
                distribution[hg-ag+bound] += prob
                if hg+ag < 2.5:
                    under_prob += prob
                else:
                    over_prob += prob
        
        distribution = distribution / np.sum(distribution)
        normalise = under_prob + over_prob
        under_prob = under_prob / normalise
        over_prob = over_prob / normalise

        awayWinProb = np.sum(distribution[: bound])
        drawProb = np.sum(distribution[bound])
        homeWinProb = np.sum(distribution[bound+1:])

        return [homeWinProb, drawProb, awayWinProb, under_prob, over_prob]
