""" Functions for evaluating a model """

from main import df, clubs
import numpy as np
import pandas as pd
from IPython.display import display, HTML
import sys
sys.path.append('../data/')


MAX_BET = 10
COMMISSION = 0.05  
t = 0.15    # threshold for deciding whether to place a bet
dict_odds = {0: 'MOHome', 1: 'MODraw', 2: 'MOAway', 3: 'UOUnder', 4: 'UOOver'}


def print_table(df):
    if isinstance(df, pd.Series):
        df = pd.DataFrame(df)
    html = df.to_html(float_format='{0:.2f}'.format)
    display(HTML(html))


def output_summary_stats(train, test):
    """ Output the statistics for a model
    on both in-sample and out-of-sample data
    and on both 1x2 and under/over 2.5 market.
    Inputs are both dictionaries containing
    the statistics.
    """

    stats = pd.DataFrame(columns=['Statistic', 'In-sample', 'Out-of-sample'])
    i = 0
    stats.loc[i] = ["No. of matches", train['num_matches'], test['num_matches']]
    i += 1
    stats.loc[i] = ["No. of bets placed", train['num_bets'], test['num_bets']]
    i += 1
    stats.loc[i] = ["No. of bets won", train['num_bets_won'], test['num_bets_won']]
    i += 1
    stats.loc[i] = ["Total Staked", train['total_staked'], test['total_staked']]
    i += 1
    profit_train = train['final_amount'] - train['total_staked'] 
    profit_test = test['final_amount'] - test['total_staked']
    stats.loc[i] = ["Profits before commission", profit_train, profit_test]
    i += 1
    profit_train -= train['commission']
    profit_test -= test['commission']
    stats.loc[i] = ["Profits after commission", profit_train, profit_test]
    i += 1
    returns_train = 100*profit_train/train['total_staked']
    returns_test = 100*profit_test/test['total_staked']
    stats.loc[i] = ["Returns (in %)", returns_train, returns_test]
    i += 1

    stats.set_index('Statistic', inplace=True)
    print_table(stats)


def place_bet(P, row):
    """" returns a list of 0/1s to signify if a bet 
    is not placed or placed for each of the 5 events
    """
    T = [0, 0, 0, 0, 0]
    for i in range(0, 5):
        p_model = P[i]
        b = row[1][dict_odds[i]] 
        p_market = 1 / b
        if p_model/p_market > 1+t:
            T[i] = 1
    return T


def fraction_bet(p_model, row, bet_index):
    """ bet index = 0 if betting on home win, 
    1 if draw and so on till 4 if over 2.5 goals
    """
    b = row[1][dict_odds[bet_index]]
    p_market = 1 / b
    f = (p_model/p_market-1) / (b-1)

    if f > 1:
        print("Error: Kelly fraction more than 1")

    return round(f, 2)


def bet_won(bet_index, row):
    if bet_index==0 and row[1]['FTR']=='H':
        return True
    elif bet_index==1 and row[1]['FTR']=='D':
        return True
    elif bet_index==2 and row[1]['FTR']=='A':
        return True
    elif bet_index==3 and (row[1]['FTHG']+row[1]['FTAG'])<2.5:
        return True
    elif bet_index==4 and (row[1]['FTHG']+row[1]['FTAG'])>2.5:
        return True
    else:
        return False


def give_stats(M, train, test):
    dict_train_1x2 = gen_stats_1x2(M, train)
    dict_test_1x2 = gen_stats_1x2(M, test)

    dict_train_UO = gen_stats_UO(M, train)
    dict_test_UO = gen_stats_UO(M, test)

    print(" ")
    print("1x2 Market")
    output_summary_stats(dict_train_1x2, dict_test_1x2)

    print(" ")
    print("UO2.5 Market")
    output_summary_stats(dict_train_UO, dict_test_UO)


def gen_stats_1x2(M, df2):
    num_matches = 0
    num_bets = 0
    total_staked = 0
    num_bets_won = 0
    commission = 0
    final_amount = 0

    # record = pd.DataFrame(columns = ['date', 'Hclub', 'Aclub', 'FTR', 'Prediction', 'Won', 'Staked', 'PnL'] )
    # rec_index = 0

    for row in df2.iterrows():
        P = M.probabilities(row[1]) 
        num_matches += 1
        T = place_bet(P, row)
        if np.sum(T[: 3]) > 0:
            for i in range(0, 3):
                if T[i] > 0:
                    num_bets += 1
                    f = fraction_bet(P[i], row, i)
                    amount_to_bet = f * MAX_BET
                    total_staked += amount_to_bet

                    # predicted = 'D'    
                    # if i==0:
                    #     predicted = 'H'
                    # elif i==2:
                    #     predicted = 'A'
                    


                    if bet_won(i, row):
                        num_bets_won += 1
                        profit = amount_to_bet * (row[1][dict_odds[i]]-1)
                        commission += COMMISSION*profit
                        final_amount += amount_to_bet+profit

    #                     record.loc[rec_index] = [row[1]['date'], row[1]['Hclub'], row[1]['Aclub'], row[1]['FTR'], predicted, 1, amount_to_bet, profit]
    #                 else: 
    #                     record.loc[rec_index] = [row[1]['date'], row[1]['Hclub'], row[1]['Aclub'], row[1]['FTR'], predicted, 0, amount_to_bet, -amount_to_bet]
    #                 rec_index += 1
    # record.to_csv('record.csv')
    
    return {
            'num_matches': num_matches, 
            'num_bets': num_bets, 
            'num_bets_won': num_bets_won, 
            'total_staked': total_staked, 
            'final_amount': final_amount, 
            'commission': commission
            }


def gen_stats_UO(M, df2):
    num_matches = 0
    num_bets = 0
    total_staked = 0
    num_bets_won = 0
    commission = 0
    final_amount = 0

    for row in df2.iterrows():
        P = M.probabilities(row[1]) 
        num_matches += 1
        T = place_bet(P, row)
        if np.sum(T[3:]) > 0:
            for i in range(3, 5):
                if T[i] > 0:
                    num_bets += 1
                    f = fraction_bet(P[i], row, i)
                    amount_to_bet = f * MAX_BET
                    total_staked += amount_to_bet

                    if bet_won(i, row):
                        num_bets_won += 1
                        profit = amount_to_bet * (row[1][dict_odds[i]]-1)
                        commission += COMMISSION*profit
                        final_amount += amount_to_bet+profit

    return {'num_matches': num_matches, 
            'num_bets': num_bets, 
            'num_bets_won': num_bets_won, 
            'total_staked': total_staked, 
            'final_amount': final_amount, 
            'commission': commission
            }


def evaluate(M):
    """ takes as input an object of class Model,
    trains the model and prints its statistics
    """
    print(M.name)

    """ split into training and test set """
    test_length = min(200, int(0.2*len(df)))
    split = len(df) - test_length
    train = df[: split]
    test = df[split:]

    """ train the model """
    M.train_model(train, clubs)

    """ generate the statistics for the model """
    give_stats(M, train, test)
