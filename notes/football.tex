\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
%\usepackage[pdflatex=true,recompilepics=auto]{gastex}
\usepackage{amsfonts,amsmath,amsthm,wrapfig}
\usepackage{comment,fullpage}
%\usepackage{todonotes}
\usepackage[noend]{algorithmic}
\usepackage[boxed]{algorithm}
\usepackage{xspace}
\usepackage{framed}
\usepackage{xcolor}
\usepackage{verbatim}
\usepackage{tikz}
\usetikzlibrary{arrows}
\usepackage{enumitem}

\newcommand{\todo}[1]{\textbf{\textcolor{blue}{#1}}}
\renewcommand*\figurename{Figure}
%\renewcommand*\figureshortname{Figure}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{assumption}[theorem]{Assumption}

%\newtheorem*{rep@theorem}{\rep@title}
%\newcommand{\newreptheorem}[2]{%
%\newenvironment{rep#1}[1]{%
% \def\rep@title{#2 \ref{##1}}%
% \begin{rep@theorem}[restated]}%
% {\end{rep@theorem}}}
%
%\newreptheorem{theorem}{Theorem}
%\newreptheorem{lemma}{Lemma}
%\newreptheorem{proposition}{Proposition}

\newcommand{\poly}{\ensuremath{\mathrm{poly}}\xspace}
\newcommand{\polylog}{\ensuremath{\mathrm{polylog}}\xspace}

\newcommand{\ALGTAB}{\hspace{1em}}
\newcommand{\ALGHEAD}{\\ \setcounter{ALC@line}{0}}
\renewcommand{\algorithmicrequire}{\textbf{Algorithm}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\newcommand{\algorithmiccommentt}[1]{\colorbox{black!10}{#1}}
\newcommand{\LineIf}[2]{ \STATE \algorithmicif\ {#1}\ \algorithmicthen\ {#2} }
\newcommand{\LineIfElse}[3]{ \STATE \algorithmicif\ {#1}\ \algorithmicthen\ {#2} \algorithmicelse\ {#3} }

\newcommand\E{\mathbb{E}}
\newcommand\e{\epsilon}
\newcommand\R{\mathbb{R}}
\newcommand\pr{\mathrm{Pr}}
\newcommand\disc{\mathrm{disc}}
\newcommand\vecdisc{\mathrm{vecdisc}}
\newcommand\pE{\mathbb{\tilde E}}
\newcommand{\ov}{\overline}
\newcommand{\tr}{\mathcal{T}}
\newcommand{\cz}{\mathcal{Z}}
\DeclareMathOperator{\dem}{dem}
\DeclareMathOperator{\MFN}{MFN}
\newcommand{\paths}{\mathcal{P}}

\newcounter{note}[section]
\renewcommand{\thenote}{\thesection.\arabic{note}}
%\renewcommand{\thenote}{\arabic{note}}
\newcommand{\emphnote}[1]{\textcolor{red}{\sf #1}}
\newcommand{\sgnote}[1]{\refstepcounter{note}\textcolor{blue}{$\ll${\bf Shashwat~\thenote:} {\sf #1}$\gg$\marginpar{\tiny\bf S~\thenote}}}


\date{\vspace{-5ex}}
\begin{document}

\title{Betting on Football}
\maketitle

In this post we study various models proposed in literature to predict the number of goals scored in a football match. We will use these to place pre-match bets on the markets for the outcome of a match and the over/under 2.5 goals market. In the first section, we will discuss various parts of our betting platform such as the data used and our investment strategy. In the next section, we will turn to study the various models for a game. 


\section*{Betting Platform}


\subsection*{Data}
We assume the data is provided in a csv file with the following columns:
\begin{itemize}
\item `date' :  The date of the match in pandas datetime format YYYY-MM-DD. 
\item `Hclub', `Aclub': The name of the home and away teams respectively. 
\item `FTHG', `FTAG': The number of goals scored by the home and away team respectively at the end of the game. 
\item `FTR': The result of the match; takes value `H' for a home win, `D' for a draw and `A' for an away win.
\item `MOHome', `MODraw', `MOAway': The odds given pre-match for the various match outcomes.
\item `UOUnder', `UOOver': The odds given pre-match for under/over 2.5 goals in total. 
\end{itemize}

The odds are expressed in decimal format minus 1. This is done in order to better reflect the profit we would get on placing a bet. For example, an odd of 0.5 means that if we place a winning bet of 10, then we will get back a profit of 5, along with our bet of 10. Various other formats for expressing odds are sometimes helpful. For example, 1.5 here is what is actually given as odds in the market. The reciprocal of $1.5 = 2/3 $ gives the probability assigned to the event according to the market. 

We will also assign a unique index to each team in the data set and for each match, add the following two columns:
\begin{itemize}
\item `Hindex': index of the home team 
\item `Aindex': index of the away team
\end{itemize}


\subsection*{Betting Markets}
We will study the following two markets:
\begin{itemize}
\item 1X2 market: this market is for whether the outcome of the match was a home-win, draw or away-team win.
\item Under/over 2.5 market: this market is for whether the total number of goals scored in the match by both teams combined is at most or more than 2 respectively. 
\end{itemize}
Everywhere, we use the odds from Betfair exchange and place only pre-match bets. In the literature there is some consensus that while the 1X2 market is quite efficient, there is some amount of under-betting on the Under 2.5 market. A conjecture to explain this is that most betters bet on an exciting match with lots of goals rather than a dull one. 

We assume a commission of 5 percent on all profits in case of winnings bets and 0 for losing bets. This is consistent with the commission on Betfair Exchange for zero discount rate. 


\subsection*{Investment Strategy}
For each event (home-win, draw, away-win, under 2.5 goals, over 2.5 goals) for each match, we must decide whether to place a bet or not and if yes, how much to bet. We use the following two rules as our investment strategy. Let $p_{model}(A), p_{market}(A)$ be the probability implied by our model and market of event $A$ occurring respectively. Let $b(A)$ be the odds associated with event $A$, that is, $(b+1) = 1/ p_{market}(A)$.
\begin{enumerate}
\item \textbf{Which bets to make:} we place a bet on $A$ if
\[ \frac{ p_{model}(A)}{ p_{market}(A)} -1 > t \]
for a properly chosen thereshold parameter $t$ eg $0.15$
\item \textbf{How much to bet:} we put a restriction of $10$ on the maximum amount to be bet on any single event. For an event $A$ on which we make a bet, we place a bet of $10 f$ where $f$ is given by
\[ f = \frac{\frac{ p_{model}(A)}{ p_{market}(A)} -1 }{b} .\]
The above comes from using Kelly's criterion to maximize the long term utility which is taken to be log-wealth.  
\end{enumerate}

\subsection*{Model Training and Validation}
We leave the minimum of last 20 percent or 200 games to test on, using the rest for training the models. We use the following metrics to evaluate our models, for both 1X2 and UO2.5 markets and for both in-sample and out-sample data:
\begin{enumerate}
\item Number of matches. 
\item Number of bets placed.
\item Number of winning bets.
\item Total staked:  this equals the total amount we put on all bets. 
\item Profits generated before commission.
\item Profits generated after commission. 
\item Returns: this equals profits generated after commission as a percentage of total amount staked. 
\end{enumerate}

\subsection*{Software Specifications}
We assume that data is already provided to us in a clean format as a csv file as mentioned in the section on Data. 

We make an abstract class Model, and each model studied will be a derived class of Model. 

\bigskip
class Model(object):

\bigskip
\qquad name // name of model

\bigskip

$\qquad$	def trainModel(dataframe df):

$\qquad\qquad$		// train the model using all the data in df

\bigskip
		
$\qquad$	def probabilities(row of a dataframe):

$\qquad\qquad$		// input corresponds to data for one match

$\qquad\qquad$		// returns a list $[p_H, p_D, p_A, p_U, p_O]$

$\qquad\qquad$		// of probabilities of home win, draw, away win, under 2.5 goals, over 2.5 goals

$\qquad\qquad$		// as given by the model

\bigskip

To evaluate the performance of a model M1, we make an object M of class M1 and then call the function evaluate(M). This generates the statistics mentioned in the previous subsection. 


\section*{Poisson Models}

We will first study the well-known Poisson distribution models. A Poisson distribution with mean $\lambda$ is defined as
\[\Pr[X=x] = e^{-\lambda}\frac{\lambda^x}{x!} \]
for $x$ an integer. The mode of $X$ is the integer $x$ for which the above probability has the maximum value and equals $\lfloor \lambda \rfloor$. If we need to make a single prediction for $X$, we will use $\lfloor \lambda \rfloor$.

\bigskip
To model the final score in a football match, for each team $i$ we define the 
following parameters:
\begin{enumerate}
\item $\alpha_i$ : represents team $i$'s attack strength; greater the $\alpha_i$, the stronger is the team's attack.  
\item $\beta_i$ : represents team $i$'s defence weakness; greater the $\beta_i$, the weaker is the team's defence.
\end{enumerate}
We also define a global parameter $\gamma$ to represent the home advantage of a team. Each parameter is non-negative. 

Suppose we know the right values of the above parameters and consider a game played between home team $i$ and away team $j$.  We will use $X$ as the random variable measuring the number of goals scored by the home team and $Y$ for the number of goals scored by the away team. In Poisson models, we assume that $X$ follows a Poisson distribution with mean \[ \lambda_X = \alpha_i\beta_j\gamma\] and $Y$ follows a Poisson distribution with mean \[ \lambda_Y = \alpha_j\beta_i.\]
For the purpose of implementation, it is better to express the above as $\ln\lambda = \alpha + \beta + \gamma$. We will work with this form when using a library for optimization. We will use the previous form when we can manually derive the solution. 

In the following subsections, we study a few models which differ in the way the parameters are learned. For each model, we will learn the values of the parameters, evaluate the accuracy of our predictions and its performance on the betting markets. 

\subsection*{Model 1 : Independent Poissons}
In this model we assume that the number of goals scored by each team is independent of the other team. That is, in a game played between home team $i$ and away team $j$, the probability that $i$ scores $x$ goals and $j$ scores $y$ goals is given by:
\[ \Pr[X=x, Y=y] = \Pr[X=x]\cdot \Pr[Y=y] \]

We learn the values of the parameters using maximum likelihood estimate. That is, we choose the parameters to maximize the value
\[ \prod_k  \Pr[X=x_k, Y=y_k] \]
where $k$ runs over all the matches in our training sample. To get a unique solution, we will impose the constraint $\prod_i \alpha_i =1$.

We can optimize the above directly (after taking logs) in python using scipy.optimize. Note that in that case, we will be working with the form $\ln\lambda = \alpha + \beta + \gamma$ and the uniqueness constraint turns into $\sum_i \alpha_i =1$.

However, this model is simple enough that we can get an algorithm to derive the parameters. We look at this approach below as it is instructive how the parameters relate to the each other and the data set. 

Let $G_i$ be the total number of goals scored by team $i$ in the given data and $G'_i$ be the total number of goals conceded by team $i$. Let $H_i$ denote the set of teams with whom team $i$ played at home. Let $A_i$ denote the set of teams with whom team $i$ played as away team. For a match $k=(i,j)$ played between home team $i$ and away team $j$, let $x_k$ denote the number of goals scored by the home team $i$ and $y_k$ the number of goals scored by away team $j$. 

By taking the derivative of the log-likelihood function, it follows that the parameters which maximize the likelihood function satisfy:

\begin{eqnarray*}
\alpha_i & = & \frac{G_i}{ \gamma \sum_{j\in H_i} \beta_j + \sum_{j\in A_i} \beta_j } \\
\beta_i & = & \frac{G'_i}{ \gamma \sum_{j\in A_i} \alpha_j + \sum_{j\in H_i} \alpha_j } \\
\gamma & = & \frac{\sum_{k} x_k }{ \sum_{k=(i,j)}  \alpha_i\beta_j }
\end{eqnarray*}

As the parameters all depend on each other, we do not get a closed-form solution. We can instead use an iterative procedure to solve the above equations:
\begin{enumerate}
\item Set $\alpha^{(0)} = \beta^{(0)} = \gamma^{(0)} = 1, \,t = 1$.
\item Repeat until convergence:
\begin{enumerate}
\item Solve the above equations with right hand side parameters given by superscript $t-1$ and right hand parameters given by superscript $t$.
\item t = t + 1.
\end{enumerate}
\item Multiply each $\alpha_i$ by $(1/K)$ and each $\beta_i$ by $K$ for $K$ such that the product of all the new $\alpha_i$'s equals $1$. 
\end{enumerate}
We can set the convergence criterion to be when none of the parameters change by more than $\epsilon$ for a small $\epsilon$, for example $0.01$. Experiments show that the above algorithm converges quickly. 



\subsection*{Model 2 : Bivariate Poissons}
In this model we assume that there is some dependence between the number of goals scored by the two teams in a match. This definitely seems more realistic as the two teams do not play separate games. We model this dependence by forcing $X$ and $Y$ to have a correlation $\rho$ for a suitable value of $\rho$. To do this, we make three independent Poisson distributions $P, Q, R$ such that
\[X = P+R  \qquad\text{and}\qquad Y = Q+R ,\]
and the means of these distributions are given by
\begin{eqnarray*}
\lambda_P &=& \lambda_X - \eta  \\
\lambda_Q &=& \lambda_Y - \eta\\
\lambda_R &=& \eta = \sqrt{\rho \lambda_X \lambda_Y}
\end{eqnarray*}

Then, 
\[ \Pr[X=x, Y=y] = \sum_{\ell}\Pr[P=x-\ell]\cdot\Pr[Q=y-\ell]\cdot\Pr[R=\ell] \]

Because of the summation involved in the above probability, it is difficult to get a solution for the maximum likelihood estimate. An approximation could be to just use the value of parameters learnt from model 1. We can get a value for $\rho$ by computing the correlation between $X$ and $Y$ from the given data. Thus, we use the correlation only for prediction; for learning the parameters we assume there is no correlation. Alternately, to directly learn the parameters we can use scipy.optimize. 

\subsection*{Model 3 : Independent Poissons + Time-Decay}

This is the similar to model 1 with the difference that while learning the parameters, we want to give more weight to recent matches. This is done because the strength of a team might change. Giving more weight to recent matches than past matches captures the current strength of the team better.  

The maximum likelihood function now takes the form
\[ \prod_{k: t_k < t} e^{-\zeta(t-t_k)}  \Pr[X=x_k, Y=y_k] \]
where $t_k$ is the time of match $k$ and $t$ is the time our training set ``ends". Thus it will make sense here to divide the training set and test set according to time. Here $\zeta >0$ is a parameter which captures how much relative weight to give to past matches. 

Using independent Poissons and taking logs, we can write the negative log-likelihood function as :
\[ NLL = \sum_k \left[ \zeta(t-t_k) -   Poissons.log(x_k,\lambda_x) -  Poissons.log(y_k,\lambda_y) \right]\]
which we want to minimize. It should be said now that $\zeta$ is a parameter which depends on the dataset and the model. Thus it is not a constant and cannot be left out of the objective function. We will see shortly the exact form of $\zeta$. We do not optimize the NLL with respect to $\zeta$ as that will only give $\zeta = 0 $.

Let us see how to choose $\zeta$ now. In literature its value is chosen as the value maximizing:
\[ f(\zeta) = \sum_k (R_H\log p_H + R_A\log p_A +  R_D\log p_D) \]
where $R_H = 1$ if $k^{th}$ match is a home win and zero otherwise. Similarly for away win and draw. $p_H$ is the probability implied by our model that home team will win and similarly for away team and draw. This can be thought as a maximum likelihood function for $\zeta$ itself. 

$p_H$ in the above is the probability of a home-win as predicted by our model. That is, we use the learned $\lambda_x$ and $\lambda_y$ from the model to predict the probability that the home team scores more goals than the away team. This brings us to a subtle point: the parameter $\zeta$ depends on the learned parameter $\lambda$ but the $\lambda$ itself depends on $\zeta$ through the NLL objective function. 

The above is thus a multi-objective optimization and can be better written as

\begin{eqnarray*}
\min_{\lambda} NLL_{\zeta}(\lambda) &=& \sum_k \left[ \zeta(t-t_k) -   Poissons.log(x_k,\lambda_x) -  Poissons.log(y_k,\lambda_y) \right] \\
\max_{\zeta} f_{\lambda}(\zeta) &=& \sum_k (R_H\log p_H + R_A\log p_A +  R_D\log p_D)
\end{eqnarray*}

where the subscript under a function denotes that the function depends on the subscript. $p_H$ is the probability that $H>A$ where $H$ is sampled from $Poisson(\lambda_x)$ and $A$ is sampled from $Poisson(\lambda_y)$.


\subsection*{Model 4 : Bivariate Poissons + Time-Decay}
This is the same as model 2 with the addition of time-decay as in model 3. 


\end{document}
